# OME research   
 
 **What is the learning goals of the education ?**

The goal with the education is to give the student the knowledge and skills to work in a leadership role in companies on land and offshore.

They will be able to take responsibility for operations and maintenance of technical installations within security, economy and environment.

**What educational level in the qualification framework is the OME education ?**

In Denmark the education is set to level 6 in the qualification framework which goes from 1-8 
in terms of life long educational level and expected outcome of the learning.

**What courses are included in the education ?**

Courses:
- Interdisciplinary elements, including methodology
- Thermal machines and systems
- Electrical and electronic machinery, plant and equipment
- Process analysis and automation
- Management, finance and security
- Professional internship for 3 months
- Bachelor project

Aside from their normal courses they have 4 optional courses in which they will pick what they wish to focus on.

- Maritim
- Energy on the Sea
- Industry and Leadership
- Data Centre Program (newly added)

**What kind of jobs does OME’s occupy ?**

Commonly seen as:
- Project Leaders
- Sales Engineer
- Environmental and Quality Manager
- Technical Manager
- Operations Manager
- Energy Consultant

**What challenges can we experience in our communication with OME’s**

The OME teams have more UCL teams connected, so they dont always have time.
And they have another schedule, so they might not have time when we have.

## Link to documentation related to the task
http://www.fms.dk/om-uddannelsen 
https://www.ug.dk/uddannelser/professionsbacheloruddannelser/tekniskeogteknologiskeudd/maritimeuddannelser/maskinmester