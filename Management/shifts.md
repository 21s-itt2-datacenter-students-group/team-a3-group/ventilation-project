Starting from week 8 we will shift master(task manager and leader) and assistant(secretary) roles

22.02.2021 - 24.02.2021: Andrei - master / Jacob - assistant     |      Week 8

01.03.2021 - 03.03.2021: Mark - master / Andrei - assistant      |      Week 9

08.03.2021 - 10.03.2021: Robert - master / Mark - assistant      |      Week 10

15.03.2021 - 17.03.2021: Gabriel - master / Robert - assistant   |      Week 11

22.03.2021 - 24.03.2021: Mate - master / Gabriel - assistant     |      Week 12

29.03.2021 - 31.03.2021:                Holiday                  |      Week 13

05.04.2021 - 07.04.2021: Allan - master / Mate - assistant       |      Week 14

12.04.2021 - 14.04.2021: Jacob - master / Allan - assistant      |      Week 15
